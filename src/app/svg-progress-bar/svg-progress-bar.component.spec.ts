import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SvgProgressBarComponent } from './svg-progress-bar.component';

describe('SvgProgressBarComponent', () => {
  let component: SvgProgressBarComponent;
  let fixture: ComponentFixture<SvgProgressBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SvgProgressBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SvgProgressBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
