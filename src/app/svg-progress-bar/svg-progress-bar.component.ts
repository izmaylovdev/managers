import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'svg-progress-bar',
  templateUrl: './svg-progress-bar.component.html',
  styleUrls: ['./svg-progress-bar.component.scss']
})
export class SvgProgressBarComponent implements OnInit {
  @ViewChild('svgElement') svgElement: ElementRef;
  @Input() stepCount: number;
  @Input() newStep: Observable<any>;
  @Input() reset: Observable<any>;

  circles: Circle[];
  progres: number;
  width: number;
  dashArr: string;
  current: number;
  colors;
  sub;
  radius: number;

  constructor() {
    this.circles = [];
    this.current = 0;
    this.progres = 0;
    this.colors = {};
    this.colors.red = '#ec1a3a';
    this.colors.green = '#9fd500';
    this.radius = 6;
   }

  ngOnInit() {
    this.width = parseInt(window.getComputedStyle(this.svgElement.nativeElement).width);
    this.radius = this.width < 500? 6: 10;
    this.progres = 0;
    this.dashArr = '0 5000';

    for(let i = 0; i < this.stepCount; i++) {
      this.circles.push(this.getCircle(i));
    }

    this.sub = this.newStep.subscribe(res => {
      this.setProgres(++this.current, res);
    });
    this.reset.subscribe(count => {
      this.circles = [];
      this.stepCount = count;
      this.current = 0;
      this.progres = 0;
      this.dashArr = '0 5000';

      for(let i = 0; i < this.stepCount; i++) {
        this.circles.push(this.getCircle(i));
      }
    })
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  getCircle(index: number) {
    let left = ((this.width-this.radius*2-1)/(this.stepCount - 1) * index)+this.radius+1,
        opacity = 0,
        borderOpacity = 0,
        dashArr = '0 100';
      
    if(index === 0){
      dashArr = '100 100';
      borderOpacity = 1;
    }

    return {
      left: left,
      cx: `${left}`,
      smallCX: `${left}`,
      fill: 'none',
      r: this.radius,
      smallR: this.radius/2,
      opacity: opacity,
      dashArr: dashArr,
      borderOpacity: borderOpacity,
      dashOffset: 0,
      transform: ''
    }
  }

  setProgres(index, res) {
    let step = this.width/((this.stepCount-1)*100);
    let i = 0,
        j = 0;

    this.circles[index - 1].fill = res? this.colors.red: this.colors.green;

    function fill() {
      setTimeout((function () {
        this.circles[this.current-1].opacity += .1;
        j++;
        this.setStrokeDasharray(this.progres);
        if(j < 10){
          fill.apply(this);
        }
      }).bind(this), 50);
    }
    fill.apply(this);

    function rec() {
      setTimeout((function () {
        this.progres += step;
        i++;
        this.setStrokeDasharray(this.progres);
        if(i < 100){
          rec.apply(this);
        }
        else {
          this.setStrokeToCircle(index);
        }
      }).bind(this), 2);
    }
    rec.apply(this);
  }
  setStrokeToCircle(index) {
    let i = 0;
    let circle: Circle = this.circles[index];
    function recCircleStorke(circle) {
        setTimeout((function () {
          circle.dashArr = `${i++ * 1} 100`;
          circle.transform = `rotate(${ 180-(180/(circle.r*6) * i) }  ${circle.left} 12)`;
          i++;
          if(i < 65){
            recCircleStorke.call(this, circle);
          }
        }).bind(this), 3);
    }
    if(circle != undefined){
      circle.borderOpacity = 1;
      circle.transform = `rotate(${ -180/80 * i }  ${circle.left + 10} 12)`;
      circle.dashArr = '0 70';

      recCircleStorke(circle);
    }
  }
  setStrokeDasharray(val) {
      this.dashArr = `${val} 5000`
  }
}


class Circle {
  left: number;
  cx: string;
  smallCX: string;
  fill: string;
  opacity: number;
  r: number;
  smallR: number;
  dashArr: string;
  borderOpacity: number;
  transform: string;

  constructor() {}
}