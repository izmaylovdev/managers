import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { IGroup } from './interfaces';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Video } from './Video';
import 'rxjs/operator/toPromise';
import 'rxjs/operator/map';


@Injectable()
export class DataService {
  path: string = 'http://tests.khromets.com/api/v1/list?email=';
  mail: string;
  obs: Promise<any>;
  data: IGroup[];
  constructor(private http: Http) {
    this.mail = this.getCookie('mail');
    this.login();
  }

  getCookie(name) {
    var matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }
  login() {
    this.obs = this.http.get(`${this.path}${this.mail}`).toPromise()
      .then(res => {
          return Promise.resolve(res.json());
      })
    return this;
  }
  loadVideos() {
    this.obs
        .then( 
          data => {
            this.data = data;
            document.getElementById('preloader').style.opacity = '0';
            setTimeout(function() {
              document.getElementById('preloader').style.display = 'none';
            },1000)
          }
        )
        .catch(
          err => {
            document.getElementById('preloader').style.opacity = '0';
            setTimeout(function() {
              document.getElementById('preloader').style.display = 'none';
            },1000)
          }
        );
    return this.obs;
  }
  setMail(mail: string) {
    document.cookie = `mail=${mail}`;
    this.mail = mail;
  }
  getMail() {
    return this.mail != undefined? this.mail: this.getCookie('mail');
  }
  getVideos() {
    return this.data;
  }
  getVideoById(id: number){
    let returned = new Video;
    if(this.data != undefined){
      this.data.forEach(group => {
        group.videos.forEach(video => {
          if (video.id === id){
            returned =  video;
          }
        });
      }) 
    }  
    return returned;
  }
  getGroupByVideoId(id: number){
    let returned;
    if(this.data != undefined){
      this.data.forEach(group => {
        group.videos.forEach(video => {
          if (video.id === id){
            returned =  group;
          }
        });
      }) 
    }  
    return returned;
  }
  saveResult(result: boolean, videoId) {
    let header = new Headers(),
        options = new URLSearchParams();

    return this.http.patch(`http://tests.khromets.com/api/v1/change?email=${this.mail}&videoId=${videoId}&testStatus=${result? '1':'0'}`,options);
  }
  getNextId(id){
    let nextId,
        groupIndex = this.data.indexOf(this.getGroupByVideoId(id)),
        videoIndex = this.data[groupIndex].videos.indexOf(this.getVideoById(id));

    if(this.data[groupIndex].videos.length-1 > videoIndex){
      return this.data[groupIndex].videos[videoIndex+1].id;
    } else if( groupIndex != this.data.length - 1 ){
      return this.data[groupIndex+1].videos[0].id;
    }
  }
  setDone(id: number) {
    let video = this.getVideoById(id),
        next = this.getVideoById(this.getNextId(id));
    video.test_status=1;
    video.video_status=2;
    next.video_status = !next.video_status? 1: next.video_status;
  }
}
