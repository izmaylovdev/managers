export interface ITest{
  questions: IQuestion[],
  maxErrors: number,
  id: number,
  test_status: number
}
export interface IQuestion{
  text: string,
  options: IOption[]
}          
interface IOption{
  text: string,
  is_corect: number
} 

export interface IGroup{
  title: string;
  description: string;
  videos: [
    {
      id: number,
      title: string,
      url: string,
      thumbnail_url: string,
      embed_url: string,
      description: string,
      text: string,
      max_errors: number,
      video_status: number,
      test_status: number,
      questions: IQuestion[],
    }
  ]
}