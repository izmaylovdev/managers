import { Component, OnInit } from '@angular/core';
import { PopupMessagerService } from '../popup-messager.service';

@Component({
  selector: 'popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {
  type: string;
  text: string;
  visibility: boolean = false;
  subscription: any;
  
  constructor(private messager: PopupMessagerService) { }

  ngOnInit() {
     this.subscription = this.messager.getEmitter();
      
      this.subscription.subscribe(item => {
        this.visibility = true;
        this.text = item.message;
        this.type = item.type;
      });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  
  close(){
    this.visibility = false;
  }
}
