import { TestBed, inject } from '@angular/core/testing';

import { PopupMessagerService } from './popup-messager.service';

describe('PopupMessagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PopupMessagerService]
    });
  });

  it('should be created', inject([PopupMessagerService], (service: PopupMessagerService) => {
    expect(service).toBeTruthy();
  }));
});
