import { IQuestion } from './interfaces';

export class Video{
    id: number;
    title: string;
    url: string;
    thumbnail_url: string;
    embed_url: string;
    description: string;
    max_errors: number;
    text: string;
    video_status: number;
    test_status: number;
    questions: IQuestion[]
     
    constructor() {}
}
