import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DataService } from '../data.service';
import { ITest } from '../interfaces';
import { Video } from '../Video';
import { Observable, Subject, Observer } from 'rxjs';
 
@Component({
  selector: 'app-test-page',
  templateUrl: './test-page.component.html',
  styleUrls: ['./test-page.component.scss']
})
export class TestPageComponent implements OnInit { 
  id: number;
  data: Video;
  text: string;
  test: ITest;
  buttonIsVisible: boolean;
  buttonCaption: string;
  sub;
  routed: Observable<any>;
  frameLoadCount: number;

  constructor(
              private router: Router, 
              private dataService: DataService, 
              private route: ActivatedRoute, 
              private _location: Location 
              ) { 
    this.buttonCaption = 'Развернуть';
    this.data = new Video();
    this.buttonIsVisible = true;
    this.routed = new Subject();
  }

  ngOnInit() {
    this.routed = Observable.create((observer: Observer<any>) => {
      this.sub = this.route.params.subscribe(params => {
        this.dataService.loadVideos().then( 
            (data) => {
              this.id = parseInt(params['id']);
              this.data = this.dataService.getVideoById(this.id);
              this.text = this.data.text; 
              this.frameLoadCount = 0;
              this.text = this.getShortText();
              this.test = { questions: this.data.questions, maxErrors: this.data.max_errors, id: this.data.id, test_status: this.data.video_status };  
              observer.next(this.test);
              if(this.data.video_status === 0){
                this.router.navigate(['/videos']);
              }
            },
            error => {
              this.router.navigate(['/']);
            }
          );
      });
    });
    
    this.routed.subscribe((t)=>{}).unsubscribe();
  }
  frameLoad(event) {
    this.frameLoadCount++;
    if(this.frameLoadCount++ === 3){
      this._location.back();
    }
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  getShortText():string {
    if( this.data.text.length > 375 ){
      this.buttonIsVisible = true;
      return this.data.text.slice(0, this.data.text.slice(0, 375).lastIndexOf(' '))+'...';
    }else{
      this.buttonIsVisible = false;
      return this.data.text;
    }
    
  }
  getFoolText():string {
    return this.data.text;
  }
  toogleText() {
    this.buttonCaption = this.buttonCaption === 'Развернуть'? 'Свернуть' : 'Развернуть';
    this.text = (this.text === this.getShortText())? this.getFoolText() : this.getShortText();
  }
}
