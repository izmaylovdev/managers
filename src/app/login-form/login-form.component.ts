import { Component, OnInit } from '@angular/core';
import { DataService } from "../data.service";
import { PopupMessagerService } from '../popup-messager.service';
import { Router } from '@angular/router';

@Component({
  selector: 'login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  placeholderText: string = 'Адрес электронной почты';
  placeholder: string = 'Адрес электронной почты';
  mail: string;
  load: boolean = false;

  constructor(private dataService: DataService, private messager: PopupMessagerService, private router: Router) { }

  ngOnInit() {
    this.dataService.loadVideos().then(
      res => {
        this.router.navigate(['/videos']);
        this.load = false;
      },
      err =>{
        
      });
  }

  focus(event) {
    this.placeholder = '';
  }

  blur(event) {
    this.placeholder = this.placeholderText;
  }

  submit(event: Event){
    this.load = true;
    this.dataService.setMail(this.mail);
    this.dataService.login().loadVideos().then(
      res => {
        this.router.navigate(['/videos']);
        this.load = false;
      },
      err =>{
        console.log(err);
        this.messager.showPopup(
          { 
            message: 'Данный e-mail не загистрирован в системе, обратитесь к администратору.',
            type: 'error' 
          }
        );
        this.load = false;
      }
    )
    
    
  }
}
