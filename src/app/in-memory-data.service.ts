import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular2-in-memory-web-api';

@Injectable()
export class InMemoryDataService implements InMemoryDbService{

  constructor() { }

  createDb() {
    let json = `[
  {
    "title": "Группа 3",
    "description": "Описание группы 3",
    "videos": [
      {
        "id": 3,
        "title": "Видео 3",
        "url": "https://www.youtube.com/watch?v=lIn8Fvcq2no",
        "thumbnail_url": "https://img.youtube.com/vi/lIn8Fvcq2no/hqdefault.jpg",
        "embed_url": "https://www.youtube.com/embed/lIn8Fvcq2no",
        "description": "Описание видео 3",
        "text": "Текст видео 3",
        "max_errors": 3,
        "video_status": 2,
        "test_status": 0,
        "questions": [
          {
            "text": "Вопрос 1",
            "options": [
              {
                "text": "Вариант 1",
                "is_correct": 0
              },
              {
                "text": "Вариант 2",
                "is_correct": 1
              },
              {
                "text": "Вариант 3",
                "is_correct": 0
              }
            ]
          },
          {
            "text": "Вопрос 2",
            "options": [
              {
                "text": "Вариант 1",
                "is_correct": 0
              },
              {
                "text": "Вариант 2",
                "is_correct": 1
              },
              {
                "text": "Вариант 3",
                "is_correct": 0
              }
            ]
          },
          {
            "text": "Вопрос 3",
            "options": [
              {
                "text": "Вариант 1",
                "is_correct": 0
              },
              {
                "text": "Вариант 2",
                "is_correct": 1
              },
              {
                "text": "Вариант 3",
                "is_correct": 0
              }
            ]
          }
        ]
      }
    ]
  },
  {
    "title": "Группа 2",
    "description": "Описание группы 2",
    "videos": [
      {
        "id": 2,
        "title": "Видео 2",
        "url": "https://www.youtube.com/watch?v=cfHx6de55vc",
        "thumbnail_url": "https://img.youtube.com/vi/cfHx6de55vc/hqdefault.jpg",
        "embed_url": "https://www.youtube.com/embed/cfHx6de55vc",
        "description": "Описание видео 2",
        "text": "Текст видео 2",
        "max_errors": 2,
        "video_status": 1,
        "test_status": 0,
        "questions": [
          {
            "text": "Вопрос 1",
            "options": [
              {
                "text": "Вариант 1",
                "is_correct": 0
              },
              {
                "text": "Вариант 2",
                "is_correct": 1
              },
              {
                "text": "Вариант 3",
                "is_correct": 0
              }
            ]
          },
          {
            "text": "Вопрос 2",
            "options": [
              {
                "text": "Вариант 1",
                "is_correct": 0
              },
              {
                "text": "Вариант 2",
                "is_correct": 1
              },
              {
                "text": "Вариант 3",
                "is_correct": 0
              }
            ]
          },
          {
            "text": "Вопрос 3",
            "options": [
              {
                "text": "Вариант 1",
                "is_correct": 0
              },
              {
                "text": "Вариант 2",
                "is_correct": 1
              },
              {
                "text": "Вариант 3",
                "is_correct": 0
              }
            ]
          }
        ]
      }
    ]
  },
  {
    "title": "Группа 1",
    "description": "Описание группы 1",
    "videos": [
      {
        "id": 4,
        "title": "Видео 4",
        "url": "https://www.youtube.com/watch?v=6ZaGJLy4pkw",
        "thumbnail_url": "https://img.youtube.com/vi/6ZaGJLy4pkw/hqdefault.jpg",
        "embed_url": "https://www.youtube.com/embed/6ZaGJLy4pkw",
        "description": "Описание видео 4",
        "text": "Текст видео 4",
        "max_errors": 1,
        "video_status": 0,
        "test_status": 0,
        "questions": [
          {
            "text": "Вопрос 1",
            "options": [
              {
                "text": "Вариант 1",
                "is_correct": 0
              },
              {
                "text": "Вариант 2",
                "is_correct": 1
              },
              {
                "text": "Вариант 3",
                "is_correct": 0
              }
            ]
          },
          {
            "text": "Вопрос 2",
            "options": [
              {
                "text": "Вариант 1",
                "is_correct": 0
              },
              {
                "text": "Вариант 2",
                "is_correct": 0
              },
              {
                "text": "Вариант 3",
                "is_correct": 1
              }
            ]
          },
          {
            "text": "Вопрос 3",
            "options": [
              {
                "text": "Вариант 1",
                "is_correct": 0
              },
              {
                "text": "Вариант 2",
                "is_correct": 0
              },
              {
                "text": "Вариант 3",
                "is_correct": 1
              }
            ]
          }
        ]
      },
      {
        "id": 1,
        "title": "Видео 1",
        "url": "https://www.youtube.com/watch?v=_10ebalBeQU",
        "thumbnail_url": "https://img.youtube.com/vi/_10ebalBeQU/hqdefault.jpg",
        "embed_url": "https://www.youtube.com/embed/_10ebalBeQU",
        "description": "Описание видео 1",
        "text": "Текст для видео 1",
        "max_errors": 3,
        "video_status": 0,
        "test_status": 0,
        "questions": [
          {
            "text": "Вопрос 1",
            "options": [
              {
                "text": "Вариант 1",
                "is_correct": 1
              },
              {
                "text": "Вариант 2",
                "is_correct": 0
              },
              {
                "text": "Вариант 3",
                "is_correct": 0
              },
              {
                "text": "Вариант 4",
                "is_correct": 0
              }
            ]
          },
          {
            "text": "Вопрос 2",
            "options": [
              {
                "text": "Вариант 1",
                "is_correct": 0
              },
              {
                "text": "Вариант 2",
                "is_correct": 0
              },
              {
                "text": "Вариант 3",
                "is_correct": 1
              },
              {
                "text": "Вариант 4",
                "is_correct": 0
              }
            ]
          }
        ]
      }
    ]
  }
]`;
let data = JSON.parse(json);


    return { data };
  }
}
