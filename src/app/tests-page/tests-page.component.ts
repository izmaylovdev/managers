import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';
import { PopupMessagerService } from '../popup-messager.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-tests-page',
  templateUrl: './tests-page.component.html',
  styleUrls: ['./tests-page.component.scss']
})
export class TestsPageComponent implements OnInit {
  data;
  constructor(private messager: PopupMessagerService, private dataService: DataService, private router: Router) { }

  ngOnInit() {
      this.dataService.loadVideos().then( 
        (data) => {
          this.data = data;
        },
        error => {
          this.router.navigate(['/']);
        }
      );
  }
  openVideo(event: Event, video) {
    if(video.video_status === 0){
      this.messager.showPopup({ message: 'Чтобы открыть доступ пройдите тест из предыдущего видео.', type: 'error' });
    } else {
      this.router.navigate(['test', video.id]);
    }
  }
}
