import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { InMemoryWebApiModule } from 'angular2-in-memory-web-api';
import { TestRoutingModule } from './test-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { TestsPageComponent } from './tests-page/tests-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { TestPageComponent } from './test-page/test-page.component';
import { SafePipe } from './safe.pipe';

import { DataService } from './data.service';
import { PopupMessagerService } from './popup-messager.service';
import { InMemoryDataService } from './in-memory-data.service';
import { PopupComponent } from './popup/popup.component';
import { SvgProgressBarComponent } from './svg-progress-bar/svg-progress-bar.component';

const appRoutes: Routes = [
  { path: '', component: LoginPageComponent },
  { path: 'videos', component: TestsPageComponent },
  { path: '**', redirectTo: '/videos' }
];


@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    TestPageComponent,
    TestsPageComponent,
    LoginPageComponent,
    LoginFormComponent,
    PopupComponent,
    SafePipe,
    SvgProgressBarComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes, { useHash: true }),
    BrowserModule,
    FormsModule,
    HttpModule,
    TestRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [ 
    DataService,
    PopupMessagerService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
