import { 
        Component, 
        OnInit, 
        Input, 
        ViewChild, 
        ElementRef, 
        EventEmitter,
        trigger,
        style,
        transition,
        animate,
        state,
        keyframes
} from '@angular/core';
import { ITest } from '../interfaces'; 
import { DataService } from '../data.service';
import { PopupMessagerService } from '../popup-messager.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
  animations: [
    trigger('question', [
      state('hidden', style({
        opacity: 0,
      })),
      state('visivle', style({
        opacity: 1,
        left: '0px'
      })),
      state('fast', style({})),
      transition('void => *', animate('600ms  linear', keyframes([
        style({ offset: 0, opacity: 0, transform: 'translateX(50px)', position: 'absolute', top: 0 }),
        style({ offset: .5, opacity: 0, transform: 'translateX(50px)', position: 'absolute', top: 0 }),
        style({ offset: .75, opacity: .5, transform: 'translateX(25px)', position: 'absolute', top: 0 }),
        style({ offset: 1, opacity: 1, transform: 'translateX(0px)', position: 'absolute', top: 0 })
      ]))),
       transition('void => fast', animate('1ms  linear', keyframes([
        style({ offset: 0, position: 'absolute', top: 0 }),
        style({ offset: 1, position: 'absolute', top: 0 })
      ]))),
       transition('fast => void', animate('0ms  linear', keyframes([
        style({ offset: 0, position: 'absolute', top: 0 }),
        style({ offset: 1, position: 'absolute', top: 0 })
      ]))),
      transition('* => void', animate('300ms 300ms linear', keyframes([
        style({ offset:  0, opacity:  1, transform: 'translateX(-0px)', position: 'absolute', top: 0 }),
        style({ offset: .3, opacity: .5, transform: 'translateX(-25px)', position: 'absolute', top: 0 }),
        style({ offset:  1, opacity:  0, transform: 'translateX(-50px)', position: 'absolute', top: 0 })
      ]))),
    ]),
    trigger('button', [
      state('default', style({ transform: 'translateX(0)' })),
      state('hidden', style({ transform: 'translateX(0)', display: 'none' })),
      transition('default => hidden', animate('200ms ease-in', keyframes([
        style({ offset:  0, opacity:  1, transform: 'translateY(0px)',  display: 'block' }),
        style({ offset: .5, opacity: .5, transform: 'translateY(10px)', display: 'block' }),
        style({ offset:  1, opacity:  0, transform: 'translateY(20px)', display: 'block' })
      ])))
    ]),
    trigger('test', [
      state('default', style({ transform: 'translateX(0)' })),
      state('hidden', style({ transform: 'translateX(0)', display: 'none' })),
      transition('hidden => default', animate('500ms ease-in', keyframes([
        style({ offset:  0, opacity:  0, transform: 'translateX(0px)',  display: 'block' }),
        style({ offset: .5, opacity: .5, transform: 'translateX(10px)', display: 'block' }),
        style({ offset:  1, opacity:  1, transform: 'translateX(20px)', display: 'block' })
      ])))
    ])
  ]
})
export class TestComponent implements OnInit {
  test: ITest;
  @Input() videoId: number;
  @Input() routed: Observable<any>;

  current: number;
  errors: number;
  resultStatus: number;
  svgLogger: Subject<any>;
  testIsFiled: boolean;
  states: string[];
  testIsVisible: boolean;
  buttonIsVisible: boolean;
  buttonState: string;
  testState: string;
  resetSvg: Subject<any>;
  answers: any[];
  questionState: string;
  questions: any[];
  nextId: number;;
  sub;

  constructor(private dataService: DataService, private messager: PopupMessagerService, private router: Router, private route: ActivatedRoute) {
    this.current = 0;
    this.errors = 0;
    this.resultStatus = -1;
    this.states = [];
    this.testIsVisible = false;
    this.buttonIsVisible = true;
    this.svgLogger = new Subject();
    this.resetSvg = new Subject();
    this.buttonState = 'default';
    this.testState = 'hidden';
    this.answers = [];
    this.questions = [];
    this.nextId = -1;
    this.test = { questions:[], maxErrors: 0, id: 0, test_status: -1 };
  }

  getWordEnd(count: number){
    let digit = parseInt(count.toString()[count.toString().length-1]);
    return digit === 0? 'ов': digit < 5? 'a': digit < 1? '': 'ов';
  }
  ngOnInit() {
    this.routed.subscribe(test => {
      this.dataService.loadVideos().then(() => {
        this.test = test;
        this.reset();
        this.nextId = this.dataService.getNextId(this.test.id);
        this.resultStatus = (this.test.test_status === 2)? 1: -1;
        this.buttonState = 'default';
        this.testState = 'hidden';
      });
    });
  }
  setAnswer(event: any, answer: any, question) {
    event.target.classList.add('answer_active');
    
    if(answer.is_correct == 1){
      this.svgLogger.next(0);
    } else{
      this.svgLogger.next(1);
      this.errors++;
    }
    this.current++;
    if( this.current === this.test.questions.length ){
      this.checkResults();
    }else{
      this.changeQuestion();
    }
  }

  changeQuestion() {
    this.questions.pop();
    this.questions.push(this.test.questions[this.current]);
  }
  checkResults() {
    this.questions.pop();
    this.dataService
      .saveResult(this.test.maxErrors >= this.errors, this.test.id)
      .subscribe( 
        resolve => {
          console.log(resolve);
          if(resolve.ok){
            if( this.test.maxErrors < this.errors ){
              this.resultStatus = 0;
            }else {
              this.resultStatus = 1;
              this.dataService.setDone(this.test.id);
            }
            this.testIsVisible = false;
          }
        },
        error => {
          this.messager.showPopup({ message: 'Что-то пошло не так. Перезагрузите страницу и попробуйте пройти тест снова.', type: 'error' });
        }
      )
  }
  startTest() {
    this.testIsVisible = true;
    this.buttonIsVisible = false;
    this.buttonState = 'hidden';
    this.testState = 'show';
  }
  reset() {
    this.current = 0;
    this.resetSvg.next(this.test.questions.length);
    this.errors = 0;
    this.resultStatus = -1;
    this.states = [];
    this.questions = [];
    this.testIsVisible = true;
    this.buttonIsVisible = false;
    this.buttonState = 'hidden';
    this.testState = 'show'; 
    this.answers = [];
    this.questionState = 'fast';
    this.questions.push(this.test.questions[0]);
    this.questionState = 'default';
  }
  goToNext(){
    this.router.navigate([ 'test', this.dataService.getNextId(this.test.id) ]);
  }
}