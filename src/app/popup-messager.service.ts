import { Injectable, EventEmitter } from '@angular/core';

interface IPopupMessage{
  message: string,
  type: string
}

@Injectable()
export class PopupMessagerService {
  event: EventEmitter <IPopupMessage> = new EventEmitter();

  constructor() {}

  showPopup( message: IPopupMessage ) {
    this.event.emit( message );
  }

  getEmitter() {
    return this.event;
  }
}
